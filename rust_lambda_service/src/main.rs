use aws_sdk_dynamodb::{model::AttributeValue, Client};
use lambda_runtime::{handler_fn, Context, Error};
use serde::{Deserialize, Serialize};
use serde_json::Value;

#[derive(Deserialize)]
struct CustomEvent {
    #[serde(rename = "firstName")]
    first_name: String,
}

#[derive(Serialize)]
struct CustomOutput {
    message: String,
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = handler_fn(func);
    lambda_runtime::run(func).await?;
    Ok(())
}

async fn func(event: Value, _: Context) -> Result<CustomOutput, Error> {
    let event: CustomEvent = serde_json::from_value(event)?;

    let config = aws_config::load_from_env().await;
    let dynamodb_client = Client::new(&config);

    let table_name = "YourTableName";
    let get_item_output = dynamodb_client
        .get_item()
        .table_name(table_name)
        .key("primaryKey", AttributeValue::S(event.first_name.clone()))
        .send()
        .await;

    let message = match get_item_output {
        Ok(output) => {
            if let Some(item) = output.item {
                // Correct usage of unwrap_or_default for an Option<String>
                let primary_key = item
                    .get("primaryKey")
                    .and_then(|v| v.as_s().ok())
                    .map(|s| s.to_string()) // Convert &str to String
                    .unwrap_or_default(); // Now correctly defaults to String::default() if None
                format!("Hello, {}!", primary_key)
            } else {
                "User not found".to_string()
            }
        }
        Err(_) => "Error accessing DynamoDB".to_string(),
    };

    Ok(CustomOutput { message })
}
