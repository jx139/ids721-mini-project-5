# Serverless Rust Microservice for AWS Lambda and DynamoDB
![pipeline status](https://gitlab.oit.duke.edu/jx139/ids721-mini-project-5/badges/main/pipeline.svg)

This project demonstrates a serverless microservice built with Rust, designed to run on AWS Lambda and interact with AWS DynamoDB. It provides a simple backend service that can receive JSON payloads, process data, and perform operations on a DynamoDB table.

## Requirements

- Rust and Cargo
- [AWS CLI](https://aws.amazon.com/cli/) configured with your AWS account
- An AWS account and basic knowledge of AWS services (Lambda, DynamoDB, IAM)

## Project Setup

1. **Clone the Repository**

    ```bash
    git clone <repository-url>
    cd rust_lambda_service
    ```

2. **Build the Project**

    Ensure you have the `x86_64-unknown-linux-musl` target installed:

    ```bash
    rustup target add x86_64-unknown-linux-musl
    ```

    Build the project with:

    ```bash
    cargo build --release --target x86_64-unknown-linux-musl
    ```

3. **Prepare the Lambda Function Package**

    After building, rename the binary to `bootstrap` and zip it:

    ```bash
    cp target/x86_64-unknown-linux-musl/release/rust_lambda_service ./bootstrap
    zip lambda.zip bootstrap
    ```

## Deployment to AWS Lambda

1. **Create a Lambda Function**

    You can create a new Lambda function via the AWS Management Console or the AWS CLI:

    ```bash
    aws lambda create-function --function-name rust_lambda_service \
      --handler bootstrap \
      --zip-file fileb://lambda.zip \
      --runtime provided.al2 \
      --role arn:aws:iam::YOUR_ACCOUNT_ID:role/YOUR_LAMBDA_EXECUTION_ROLE \
      --environment Variables={RUST_BACKTRACE=1}
    ```

    Make sure to replace `YOUR_ACCOUNT_ID` and `YOUR_LAMBDA_EXECUTION_ROLE` with your actual values.

    ![create](./pic/create.png)
    ![lambda](./pic/lambda.png)

2. **Set Up DynamoDB**

    - Create a new DynamoDB table through the AWS Management Console.
    - Ensure the Lambda function's IAM role has permissions to access the DynamoDB table.
    ![table](./pic/table.png)

## Testing the Lambda Function

Invoke your Lambda function through the AWS CLI, SDKs, or AWS Management Console with a test event matching the expected input format of your Lambda function.

```bash
aws lambda invoke --function-name rust_lambda_service \
  --payload '{"firstName":"John"}' \
  outputfile.txt
```
![output](./pic/output.png)
